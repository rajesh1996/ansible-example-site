This directory contains an example site inventory and playbooks for 
installing/configuring Æ-DIR servers (providers and consumers)
with _ansible_.

Please see the
[Æ-DIR installation docs](https://www.ae-dir.com/install.html)
for detailed instructions how to use this.
