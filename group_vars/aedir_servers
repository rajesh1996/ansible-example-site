#-----------------------------------------------------------------------
# vars for role aehostd
# see all defaults: https://gitlab.com/ae-dir/ansible-aehostd/-/blob/master/defaults/main.yml
#-----------------------------------------------------------------------

# these LDAP URIs are tried first in this order no matter what
aehostd_uri_list:
  # FIX ME: Variable openldap_ldapi_uri should be used but
  # we only know the hard-coded value in role aehostd
  - "ldapi://%2Fopt%2Fae-dir%2Frun%2Fslapd%2Fldapi"
# these LDAP URIs are tried after all in ldap_uri_list failed
# (to balance load this list will be rotated based on FQDN hash)
aehostd_uri_pool: "{{ groups.aedir_consumers | map('regex_replace', '^(.+)$', 'ldaps://\\1') | list }}"

# assume role ae-dir-server already installed a virtual env
aehostd_venv_path: "/opt/ae-dir"

# Cache TTL (seconds) of PAM authc results
# Default: -1 (disabled)
aehostd_pam_authc_cache_ttl: 300

# Arguments used by pam_mkhomedir for automatic creation of user's home directory
# (empty string disables use of pam_mkhomedir)
aehostd_pam_mkhomedir_args: "umask=0077 skel=/etc/skel"

#-----------------------------------------------------------------------
# vars for role aedir_server
# see all defaults: https://gitlab.com/ae-dir/ansible-ae-dir-server/-/blob/master/defaults/main.yml
#-----------------------------------------------------------------------

# base DN / search root / naming context for AE-DIR entries
# Note: Do yourself a favour and keep it short ;-)
# Default: ou=ae-dir
#aedir_suffix: "dc=ae-dir,dc=example,dc=org"

# always map these sub-trees to {{ aedir_suffix }}
#aedir_fake_search_roots:
#  - "ou=users"
#  - "ou=groups"

# All relevant aeSrvGroup entries in set-based who clauses for performance tuning:
# support supplemental service groups, but no support for proxy service groups
#aedir_who_srvgroup: "(user/-1 | user/aeSrvGroup)"
# only support for direct parent service groups, no supplemental or proxy service groups
aedir_who_srvgroup: "(user/-1)"
# full features including
#aedir_who_srvgroup: "(user/-1 | user/aeSrvGroup | user/-1/aeProxyFor)"

# enable OATH-LDAP (with external bind listeners in slapd.conf)
# set to True to enable (default is False=disabled)
oath_ldap_enabled: True

# enable mandatory access control with AppArmor on suitable platforms
apparmor_enabled: "{{ ansible_apparmor.status == 'enabled' }}"

# Various database-specific tuning parameters
# maxsize: see directive 'maxsize' in slapd-mdb(5)
# checkpoint: see directive 'checkpoint' in slapd-mdb(5)
# syncprov_sessionlog: see directive 'syncprov-sessionlog' in slapo-syncprov(5)
# syncprov_checkpoint: see directive 'syncprov-checkpoint' in slapo-syncprov(5)
# sizelimit: see directive 'sizelimit' in slapd.conf(5)
# timelimit: see directive 'timelimit' in slapd.conf(5)
openldap_db_params:
  accesslog:
    mdb_maxsize: 500000000
    checkpoint: "5000 2"
    syncprov_sessionlog: 10000
    syncprov_checkpoint: "5000 2"
    sizelimit: -1
    timelimit: 120
  um:
    mdb_maxsize: 100000000
    checkpoint: "1000 1"
    syncprov_sessionlog: 10000
    syncprov_checkpoint: "1000 1"
    sizelimit: 500
    timelimit: 60
  session:
    mdb_maxsize: 10000000
    checkpoint: "20000 10"
    syncprov_sessionlog: 1000
    syncprov_checkpoint: "20000 10"
    sizelimit: 2
    timelimit: 10

# number of file descriptors for LDAP connections
openldap_limit_nofile: 128

# which messages to send to syslog
# normal for production: stats
openldap_log_level: "stats sync"

# how many worker threads are started
# RAM needed per thread: 4MB on 32 bit systems, 8MB on 64 bit systems
# rule-of-thumb: 4 per real CPU core
#openldap_threads: 8

# Subject DN suffix
openldap_tls_cert_suffixes:
  - "OU=Example OU,O=Example O,L=Example L,ST=Example ST,C=CO"

# Directory for metrics text exports picked up by Prometheus node-exporter.
# If empty, no text exporter CRON jobs are installed.
# Setting this to empty string disables dumping metrics to files.
# Example:
# aedir_metricsdir: "/var/spool/metrics"
aedir_metricsdir: ""

# Group ID or name for setting group ownership of metrics directory and files
# to grant at least read access to directory specified by aedir_metricsdir.
# This group has to already exist on the system!
#aedir_metrics_owner_group: "prometheus"

#---------------------------------------------------------------------------
# password sync parameters
#---------------------------------------------------------------------------

# regex pattern for limiting the entries for which password changes are exported
#aedir_pwsync_dn_regex: "^uid=({{ aedir_aeuser_uid_regex }}|{{ aedir_aeservice_uid_regex }}),cn={{ aedir_base_zone }},{{ aedir_suffix }}$"

# password of authenticating to sync target (should be ansible vault or similar)
#aedir_pwsync_targetpassword: "foobar"

# The CA certificate file bundle for connecting to sync target
#aedir_pwsync_cacert_filename:  "{{ openldap_cacert_filename }}"

#---------------------------------------------------------------------------
# Apache httpd parameters
#---------------------------------------------------------------------------

# Path name of CA cert bundle file
apache_cacert_filename: "{{ openldap_cacert_filename }}"

#-----------------------------------------------------------------------
# vars slapd_check
#-----------------------------------------------------------------------

# fully-qualified domain name of local LDAPS service
#slapd_check_service_fqdn: "{{ openldap_service_fqdn }}"

# expected authz-ID for local LDAPS connection
#slapd_check_authz_id: "dn:uid=ae-dir-slapd_{{ slapd_check_service_fqdn }},cn=ae,{{ aedir_suffix }}"

#-----------------------------------------------------------------------
# vars for role mtail
#-----------------------------------------------------------------------

# directory path where mtail programs are located
#mtail_exec: "/usr/local/sbin/mtail"

# directory path where mtail programs are located
#mtail_progs: "/etc/mtail"
